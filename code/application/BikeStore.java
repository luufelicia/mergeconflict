//Felicia Luu
//2237149
package application;

import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args){
        Bicycle[] bicycleArray = new Bicycle[4];
        bicycleArray[0] = new Bicycle("Specialized", 21, 40);
        bicycleArray[1] = new Bicycle("Hercules", 22, 50);
        bicycleArray[2] = new Bicycle("Kettler", 23, 60);
        bicycleArray[3] = new Bicycle("Murray", 24, 30);

        for (Bicycle bicycle : bicycleArray){
            System.out.println(bicycle);
        }
    }
}
