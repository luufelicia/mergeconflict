//Felicia Luu
//2237149
package vehicles;

public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;


    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String getManufacturer(){
        return this.manufacturer;
    }

    public int getNumberGears(){
        return this.numberGears;
    }

    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    public String toString(){
        return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", MaxSpeed: " + this.maxSpeed;
    }
}